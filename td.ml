type cell = [ `Neutral | `Player1 | `Player2 ];;

type macro_cell = [ `Neutral | `Player1 | `Player2 | `Active | `Draw ];;

type game_info =
 {
   mutable timebank : int;
   mutable time_per_move : int;
   mutable player_names : string list;
 }
;;

type game_state =
 {
   mutable board : cell array array;
   mutable macroboard : macro_cell array array;
   mutable round : int;
   mutable move : int;
   setup : game_info;
 }
;;

type player =
 {
  id: int;
  cmd : string;
  timebank_remaining : int;
 }
;;

type game =
 {
  player1 : player;
  player2 : player;
  model : game_state;
 }
;;

