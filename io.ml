open Td;;

(* tokenizer from rosetta code *)
let split_char sep str =
  let string_index_from i =
    try Some (String.index_from str i sep)
    with Not_found -> None
  in
  let rec aux i acc = match string_index_from i with
    | Some i' ->
        let w = String.sub str i (i' - i) in
        aux (succ i') (w::acc)
    | None ->
        let w = String.sub str i (String.length str - i) in
        List.rev (w::acc)
  in
  aux 0 []
;;

let three_token t1 t2 t3 =
  (int_of_string t3), (int_of_string t2) 
;;

let parse_order line =
  let tokens = split_char ' ' line in
  match List.length tokens with
  | 3 -> three_token (List.nth tokens 0) (List.nth tokens 1) (List.nth tokens 2)
  | _ -> failwith ("incorrect bot input\n" ^ line)
;;

let input_order chan = 
  let line = input_line chan in 
    parse_order line 
;; 

