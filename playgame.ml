open Td;;

class runner in_chan out_chan pid stdin_in stdin_out stdout_in stdout_out bot_stderr (bot_id:int) =
  object (self)
  method send_data v = 
    output_string out_chan v;
    flush out_chan;
  method read_order =
    bot_id, (Game.read_order in_chan)
  method kill =
    Unix.kill pid 9;
    Unix.close bot_stderr;
    Unix.close stdin_in;
    Unix.close stdin_out;
    Unix.close stdout_in;
    Unix.close stdout_out;
  method get_bot_id = bot_id
  end
;;

let bot_stderr_file bot =
  let name = Printf.sprintf "%d" bot.id in
    "logs/player_" ^ name ^ "_stderr.log"
;;

let run_bot bot =
  let stdin_in, stdin_out = Unix.pipe () in
  let stdout_in, stdout_out = Unix.pipe () in
  let bot_stderr = Unix.descr_of_out_channel (open_out (bot_stderr_file bot)) in
  let pid = Unix.create_process bot.cmd [||] stdin_in stdout_out bot_stderr in
  let out_chan = Unix.out_channel_of_descr stdin_out in
  let in_chan = Unix.in_channel_of_descr stdout_in in
    new runner in_chan out_chan pid stdin_in stdin_out stdout_in stdout_out bot_stderr bot.id
;;

let run_game game =
  let p1 = run_bot game.player1 in
  let p2 = run_bot game.player2 in
  p1#send_data (Game.render_setup game.model.setup game.player1);
  p2#send_data (Game.render_setup game.model.setup game.player2);

  let players = [| p1; p2 |] in
  while not (Game.finished game.model) do
    Game.text_board game.model.board;
    Game.text_macroboard game.model.macroboard;
    let player = 
      if game.model.move mod 2 = 0 then game.player2 else game.player1 
    in
    let runn = players.((game.model.move + 1) mod 2) in
    runn#send_data (Game.render game.model player);
    let order = runn#read_order in
      Game.update game.model order;
      Debug.log (Printf.sprintf "turn = %d\n" game.model.move);
      Debug.log (Printf.sprintf "player = %d\n" player.id);
      Debug.log (Printf.sprintf "runner = %d\n" runn#get_bot_id);
      flush stderr;
      flush stdout;
      game.model.move <- game.model.move + 1;
      game.model.round <- game.model.move / 2;
  done;
  p1#kill;
  p2#kill;
  Game.text_board game.model.board;
  Game.text_macroboard game.model.macroboard;
;;

let init bot_cmd1 bot_cmd2 =
  let m = Game.new_state bot_cmd1 bot_cmd2 in
  let player1 = Game.new_player bot_cmd1 1 m.setup.timebank in
  let player2 = Game.new_player bot_cmd2 2 m.setup.timebank in
  let g =
   {
    player1 = player1;
    player2 = player2;
    model = m;
   }
  in
    run_game g;
    let s1, s2 = Game.score_game g.model in
    Debug.log (Printf.sprintf "final score:\n player 1: %d\n player 2: %d\n" (s1) (s2));
;;

let () =
  try
    init Sys.argv.(1) Sys.argv.(2)
  with e -> failwith (Printf.sprintf "Failed with exception %s \n\n usage: \n  playgame bot_cmd1 bot_cmd2\n\n" (Printexc.to_string e))
;;

