open Td;;

let square_size = 3;;

let victory_lines = [|
  ((0,0), (0, 1), (0, 2));
  ((1,0), (1, 1), (1, 2));
  ((2,0), (2, 1), (2, 2));
  ((0,0), (1, 0), (2, 0));
  ((0,1), (1, 1), (2, 1));
  ((0,2), (1, 2), (2, 2));
  ((0,0), (1, 1), (2, 2));
  ((0,2), (1, 1), (2, 0));
|]
;;

let player_name = function
| `Neutral -> "Neutral"
| `Player1 -> "Player1"
| `Player2 -> "Player2"
;;

let empty (tile, owner) =
  tile = `Neutral
;;

let enemy id (tile, owner) =
  not ((tile = `Neutral) || (owner = id))
;;

let whose_turn state =
  2 - (state.move mod 2)
;;

let is_legal gstate row col =
  let mbr = row / square_size in
  let mbc = col / square_size in
    (gstate.macroboard.(mbr).(mbc) = `Active) 
    && (gstate.board.(row).(col) = `Neutral)
;;

let legal_moves gstate =
  let result = ref [] in
  let size = (square_size * square_size) - 1 in
  for row = 0 to size do
    begin for col = 0 to size do
      if is_legal gstate row col then result := (row, col) :: !result;
    done end
  done;
  !result
;;

let clear_active mb =
  Array.iter (fun r -> (Array.iteri (fun i c ->
    match c with
    | `Active -> r.(i) <- `Neutral
    | _ -> ()
  )) r) mb
;;

let cell_is_full board row col =
  let offr = row * square_size in
  let offc = col * square_size in
  let limit = square_size - 1 in
  let result = ref true in
  for cr = 0 to limit do
    for cc = 0 to limit do
      result := !result && (board.(offr + cr).(offc + cc) != `Neutral)
    done
  done;
  !result
;;

let type_cell_to_macrocell:cell->macro_cell = function
| `Neutral -> `Neutral
| `Player1 -> `Player1
| `Player2 -> `Player2
;;

let type_macrocell_to_cell:macro_cell->cell = function
| `Neutral | `Draw | `Active -> `Neutral
| `Player1 -> `Player1
| `Player2 -> `Player2
;;

let sub_board_status (gstate:game_state) (row:int) (col:int) : macro_cell =
  let offr = row * square_size in
  let offc = col * square_size in
  let win = Array.fold_left (fun acc ((r0, c0), (r1, c1), (r2, c2)) ->
    if acc = (`Neutral:macro_cell) then
      let p0 = type_cell_to_macrocell gstate.board.(offr + r0).(offc + c0) in
      let p1 = type_cell_to_macrocell gstate.board.(offr + r1).(offc + c1) in
      let p2 = type_cell_to_macrocell gstate.board.(offr + r2).(offc + c2) in
        if p0 != (`Neutral:macro_cell) && p0 = p1 && p0 = p2 then p0 
        else (`Neutral:macro_cell) 
    else acc
  ) (`Neutral:macro_cell) victory_lines in
  if win = `Neutral && cell_is_full gstate.board row col then `Draw else win
;;

let activate_if_not_full board row col =
  if cell_is_full board row col then `Neutral else `Active
;;

let activate_all board mb =
  Array.iteri (fun ir r -> (Array.iteri (fun ic c ->
    match c with
    | `Neutral -> r.(ic) <- activate_if_not_full board ir ic
    | _ -> ()
  )) r) mb
;;

(*
let check_draw board row col =
  if cell_is_full board row col then `Draw else `Neutral
;;

let mark_draws board mb =
  Array.iteri (fun ir r -> (Array.iteri (fun ic c ->
    match c with
    | `Neutral -> r.(ic) <- check_draw board ir ic
    | _ -> ()
  )) r) mb
;;
*)

let player_cell = function
| 1 -> `Player1
| _ -> `Player2
;;

let update_macroboard gstate row col =
  clear_active gstate.macroboard;
  let ur = row / square_size in
  let uc = col / square_size in
  gstate.macroboard.(ur).(uc) <- 
    sub_board_status gstate ur uc;
  let mbr = row mod square_size in
  let mbc = col mod square_size in
  match gstate.macroboard.(mbr).(mbc) with
  | `Neutral -> gstate.macroboard.(mbr).(mbc) <- `Active
  | _ -> activate_all gstate.board gstate.macroboard

;;

let move gstate player (row, col) =
  if is_legal gstate row col then (
    gstate.board.(row).(col) <- player_cell(player);
    update_macroboard gstate row col;
  ) else 
    let playername = player_name (player_cell player) in
    failwith (Printf.sprintf "invalid move: %s %d %d \n" playername row col)
;;

let update state (id, order) =
  let valid = legal_moves state in
(*
  Debug.log (Printf.sprintf "valid orders for player %d:\n" id);
  Debug.log_orders valid;
*)
(*  mark_draws state.board state.macroboard; *)
  if List.exists (fun x -> x = order) valid then
    move state id order
  else 
   (
    let (sr, sc) = order in 
      failwith (Printf.sprintf "invalid move: %d %d \n" sr sc )
   )
;;

let new_player cmd id timebank =
 {
  id = id;
  cmd = cmd;
  timebank_remaining = timebank;
 }
;;

let render_setup setup bot =
  let your_bot = bot.cmd in
  let your_botid = bot.id in
  Printf.sprintf "settings timebank %i\nsettings time_per_move %i\nsettings player_names %s\nsettings your_bot %s\nsettings your_botid %i\n" 
    setup.timebank setup.time_per_move (String.concat "," setup.player_names) your_bot your_botid
;;

let render_board board =
  let l = Array.fold_left (fun acc row -> Array.append acc (Array.map (function
  | `Neutral -> "0"
  | `Player1 -> "1"
  | `Player2 -> "2"
  ) row)) [| |] board in
  String.concat "," (Array.to_list l)
;;

let text_cell = function
| `Neutral -> "+ "
| `Player1 -> "o "
| `Player2 -> "x "
;;

let text_board board =
  Array.iter (fun row ->
    print_string "\n";
    Array.iter (fun cell ->
      print_string (text_cell cell)
    ) row
  ) board;
  print_string "\n\n"
;;

let text_macrocell = function
| `Neutral -> ". "
| `Player1 -> "o "
| `Player2 -> "x "
| `Active -> "! "
| `Draw -> "- "
;;

let text_macroboard macroboard =
  print_string "text_macroboard\n";
  Array.iter (fun row ->
    print_string "\n";
    Array.iter (fun cell ->
      print_string (text_macrocell cell)
    ) row
  ) macroboard;
  print_string "\n\n"
;;

let render_macroboard macroboard =
  let l = Array.fold_left (fun acc row -> Array.append acc (Array.map (function
  | `Neutral | `Draw-> "0"
  | `Player1 -> "1"
  | `Player2 -> "2"
  | `Active -> "-1"
  ) row)) [| |] macroboard in
  String.concat "," (Array.to_list l)
;;

let render game bot =
  Printf.sprintf "update game round %i\nupdate game move %i\nupdate game field %s\nupdate game macroboard %s\naction move %i\n" 
   game.round game.move (render_board game.board) (render_macroboard game.macroboard) 10000
;;

let read_order chan =
  Io.input_order chan
;;

let new_board () =
   let size = square_size * square_size in
   Array.make_matrix size size `Neutral
;;

let new_macroboard () =
   let size = square_size in
   Array.make_matrix size size `Active
;;

let new_state pname1 pname2  =
  let info = {
    timebank = 10000;
    time_per_move = 500;
    player_names = [pname1; pname2];
  } in
  let state =
   {
    board = new_board ();
    macroboard = new_macroboard ();
    round = 0;
    move = 0;
    setup = info;
   }
  in
    state
;;

let macroboard_status gstate =
 (* Active status should be cleared first *)
  Array.fold_left (fun acc ((r0, c0), (r1, c1), (r2, c2)) ->
    if acc = `Neutral then
      let p0 = gstate.macroboard.(r0).(c0) in
      let p1 = gstate.macroboard.(r1).(c1) in
      let p2 = gstate.macroboard.(r2).(c2) in
        if p0 != `Neutral && p0 != `Draw && p0 != `Active&& p0 = p1 && p0 = p2 then p0 else `Neutral
    else acc
  ) `Neutral victory_lines
;;

let none_active state =
  let one_active = ref false in
  for row = 0 to square_size - 1 do
    for col = 0 to square_size - 1 do
      one_active := !one_active || (sub_board_status state row col = `Neutral && not (cell_is_full state.board row col)) 
    done
  done;
  not !one_active
  
;;

let finished state = 
  match macroboard_status state with 
  | `Neutral | `Active -> none_active state
  | _ -> true
;;

let score_game state =
  match macroboard_status state with 
  | `Neutral | `Active | `Draw -> (0, 0)
  | `Player1 -> (1, 0)
  | `Player2 -> (0, 1)
;;


