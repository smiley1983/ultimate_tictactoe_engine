open Td;;

let get_time () = Unix.gettimeofday ();;

let square_size = 3;;

let out_chan = stderr (* open_out "mybot_err.log" *);;

let debug s =
   output_string out_chan s; 
   flush out_chan
;;

let text_cell = function
| `Neutral -> "+ "
| `Mine -> "o "
| `Theirs -> "x "
;;

let text_board board =
  Array.iter (fun row ->
    debug "\n";
    Array.iter (fun cell ->
      debug (text_cell cell)
    ) row
  ) board;
  debug "\n\n"
;;

let text_macrocell = function
| `Neutral -> ". "
| `Mine -> "o "
| `Theirs -> "x "
| `Active -> "! "
;;

let text_macroboard macroboard =
  debug "text_macroboard\n";
  Array.iter (fun row ->
    debug "\n";
    Array.iter (fun cell ->
      debug (text_macrocell cell)
    ) row
  ) macroboard;
  debug "\n\n"
;;

(* input processing *)

let owner gstate i =
  if (i = gstate.setup.your_botid) then `Mine
  else `Theirs
;;

let int_to_cell gstate i =
  debug ("owner = " ^ string_of_int i ^ "\n");
  debug ("botid = " ^ string_of_int gstate.setup.your_botid ^ "\n");
  match i with
  | 0 -> `Neutral
  | n -> owner gstate i
;;

let int_to_square gstate i =
  match i with
  | (-1) -> `Active
  | 0 -> `Neutral
  | n -> owner gstate i
;;

let uncomment s =
  try String.sub s 0 (String.index s '#')
  with Not_found -> s
;;

let new_board () =
   let size = square_size * square_size in
   Array.make_matrix size size `Neutral
;;

let new_macroboard () =
   let size = square_size in
   Array.make_matrix size size `Active
;;

let clear_gstate gstate =
 (
  gstate.board <- new_board ();
  gstate.macroboard <- new_macroboard ();
  gstate.round <- 0;
  gstate.move <- 0;
 )
;;

let split_char sep str =
  let string_index_from i =
    try Some (String.index_from str i sep)
    with Not_found -> None
  in
  let rec aux i acc = match string_index_from i with
    | Some i' ->
        let w = String.sub str i (i' - i) in
        aux (succ i') (w::acc)
    | None ->
        let w = String.sub str i (String.length str - i) in
        List.rev (w::acc)
  in
  aux 0 []

let update_game_board (gstate:game_state) data =
   (* Assume that this coincides approximately with start of turn *)
   gstate.last_update <- get_time();
   let size = square_size * square_size in
   List.iteri (fun i (c:string) ->
      let row = i / size in
      let col = i mod size in
         gstate.board.(row).(col) <- int_to_cell gstate (int_of_string c)
   ) (split_char ',' data) (* (Str.split (Str.regexp ",") data) *)
;;

let update_game_macroboard (gstate:game_state) data =
   List.iteri (fun i (c:string) ->
      let row = i / square_size in
      let col = i mod square_size in
         gstate.macroboard.(row).(col) <- int_to_square gstate (int_of_string c)
   ) (split_char ',' data ) (* (Str.split (Str.regexp ",") data) *)
;;

let action_move bot gstate t2 =
  debug "action_move\n";
  (text_board gstate.board);
  (text_macroboard gstate.macroboard);
  gstate.last_timebank <- (int_of_string t2);
  bot gstate
;;

let three_term (gstate:game_state) key t1 t2 t3 =
   if (t3 = "") || (t2 = "") || (t1 = "") || (key = "")  then raise Not_found else
   match key with
    | "update" ->
      begin match t1 with
       | "game" ->
         begin match t2 with
	  | "round" -> gstate.round <- int_of_string t3
	  | "move" -> gstate.move <- int_of_string t3
	  | "field" -> update_game_board gstate t3
	  | "macroboard" -> update_game_macroboard gstate t3
	  | _ -> ()
         end
       | _ -> ()
      end
    | _ -> ()
;;

let two_term bot gstate key t1 t2 =
   if (t2 = "") || (t1 = "") || (key = "")  then raise Not_found else
   match key with
    | "settings" -> 
      begin match t1 with
       | "timebank" -> gstate.setup.timebank <- int_of_string t2
       | "time_per_move" -> gstate.setup.time_per_move <- int_of_string t2
       | "player_names" -> gstate.setup.player_names <- split_char ',' t2 (* Str.split (Str.regexp ",") t2 *)
       | "your_bot" -> gstate.setup.your_bot <- t2
       | "your_botid" -> gstate.setup.your_botid <- int_of_string t2
       | _ -> ()
      end
    | "action" -> 
      begin match t1 with
       | "move" -> action_move bot gstate t2
       | _ -> ()
      end
    | _ -> ()
;;

let sscanf_cps fmt cont_ok cont_fail s =
  try Scanf.sscanf s fmt cont_ok
  with _ -> cont_fail s
;;

let process_line bot gstate line =
   sscanf_cps "%s %s %s %s" (three_term gstate)
     (
      sscanf_cps "%s %s %s" (two_term bot gstate)
        (fun _ -> ())
     )
     (uncomment line)
;;

let read_lines bot gstate =
  while true do
    debug "try reading line\n";
    let line = read_line () in
      debug (line ^ " : line read\n");
      process_line bot gstate line;
  done
;;

(* End input section *)

(* output section *)

let issue_order (row, col) =
   debug "issue_order\n";
   Printf.printf "place_move %d %d\n" row col;
   flush stdout;
;;

(* End output section *)

(* Utility functions *)

let random_from_list lst =
  let len = List.length lst in
    List.nth lst (Random.int len)
;;

let is_legal gstate row col =
  let mbr = row / square_size in
  let mbc = col / square_size in
    (gstate.macroboard.(mbr).(mbc) = `Active) 
    && (gstate.board.(row).(col) = `Neutral)
;;

let legal_moves gstate =
  let result = ref [] in
  let size = (square_size * square_size) - 1 in
  for row = 0 to size do
    begin for col = 0 to size do
      if is_legal gstate row col then result := (row, col) :: !result;
    done end
  done;
  !result
;;

let time_elapsed_this_turn gstate =
  (get_time() -. gstate.last_update) *. 1000.
;;

let time_remaining gstate =
  (float_of_int gstate.last_timebank -. time_elapsed_this_turn gstate)
;;

(* End utility *)

let run_bot bot =
  (* Remove or change this if you want predictable random sequences *)
  Random.self_init (); 
  let game_info =
     {
      timebank = 0;
      time_per_move = 0;
      player_names = [];
      your_bot = "";
      your_botid = -1;
     }
  in
  let game_state =
     {
      board = [|[| |]|];
      macroboard = [|[| |]|];
      round = 0;
      move = 0;
      setup = game_info;
      last_update = 0.0;
      last_timebank = 0;
     }
  in
  clear_gstate game_state;
  debug "bot init done\n";

  begin try
   (
     read_lines bot game_state
   )
  with exc ->
   (
    debug (Printf.sprintf
       "Exception in turn %d :\n" game_state.round);
    debug (Printexc.to_string exc);
    raise exc
   )
  end;
;;

(*
let this_bot gstate time_remaining = 
  issue_order (0,0)
;;

run_bot this_bot;;
*)

