open Td;;

let main gstate =
  let moves = Ulttt.legal_moves gstate in
  let move = Ulttt.random_from_list moves in
    Ulttt.issue_order move
;;

Ulttt.run_bot main

